console.log("Cold start");

exports.apiHandler = async (event, context) => {
  if (event.source === "serverless-plugin-warmup") {
    console.log("WarmUp - Lambda is warm!");
    return "Lambda is warm!";
  }

  const { appHandler } = await import("./src/index.mjs");
  return appHandler(event, context);
};

exports.thumbnailHandler = async (event, context) => {
  const { thumbHandler } = await import("./src/index.mjs");
  return thumbHandler(event, context);
};
