import express from "express";
import { DeleteObjectsCommand, S3Client } from "@aws-sdk/client-s3";

import queries from "./lib/dbQueries.mjs";
import { verifyToken } from "./lib/jwt.mjs";
import { generateThumbnailImageName } from "./lib/generateThumbnails.mjs";
import {createInvalidation} from "./lib/cloudfront.mjs";

const router = express.Router();
const filesTableName = process.env.FILES_TABLE;
const bucketName = process.env.FILE_UPLOAD_BUCKET;

router.post("/", verifyToken, async (req, res) => {
  const filename_video = req.body.videoUrl.substring(
    req.body.videoUrl.lastIndexOf("/") + 1
  );
  const key_video = "videos/" + filename_video;
  const key_thumb = "videos/" + generateThumbnailImageName(filename_video, 0);

  const clientParams = {
    region: process.env.AWS_REGION,
  };
  const s3c = new S3Client(clientParams);

  // Delete the object from S3...
  const s3Params = {
    Bucket: bucketName,
    Delete: {
      Objects: [{ Key: key_video }, { Key: key_thumb }],
    },
  };

  const command = new DeleteObjectsCommand(s3Params);
  try {
    const { Deleted } = await s3c.send(command);
    console.log(
      `Successfully deleted ${Deleted.length} objects from S3 bucket. Deleted objects:`
    );
    console.log(Deleted.map((d) => ` • ${d.Key}`).join("\n"));

    // invalidate cloudfront cache
    await createInvalidation(process.env.CLOUDFRONT_DISTRIBUTION_ID, "/api/getvideos/active");

  } catch (err) {
    console.error("deleteVideo.mjs->router.post(S3)->err: ", err);
    res.status(500).json({ message: "Failed to delete from S3: ", err });
  }

  // Delete the entry in the database...
  const dbParams = {
    TableName: filesTableName,
    Key: {
      dateString: req.body.dateString,
    },
  };
  try {
    await queries.deleteItem(dbParams);
    res.status(200).json({ message: "Deleted Successfully!" });
  } catch (err) {
    console.error("deleteVideo.mjs->router.post(DB)->err: ", err);
    res.status(500).json({ message: "Failed to delete database entry: ", err });
  }
});

export default router;
