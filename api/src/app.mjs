import dotenv from "dotenv";

import cors from "cors";
import express from "express";
import login from "./login.mjs";
import getSignedUrl from "./lib/getSignedURL.mjs";
import deleteVideo from "./deleteVideo.mjs";
import toggleVideo from "./toggleVideo.mjs";
import getVideos from "./getVideoData.mjs";
import updateDatabase from "./updateDatabase.mjs";

dotenv.config();
const app = express();

app.use(express.urlencoded({ extended: false }));
app.use(express.json({ limit: "50mb" }));

app.options("*", cors());
app.use("*", cors());
app.use("/api/login", login);
app.use("/api/getsignedurl", getSignedUrl);
app.use("/api/getvideos", getVideos);
app.use("/api/updatedatabase", updateDatabase);
app.use("/api/deletevideo", deleteVideo);
app.use("/api/togglevideo", toggleVideo);

app.use((req, res, next) => {
  return res.status(404).json({
    error: "Not Found",
  });
});

app.listen(3000);

export default app;
