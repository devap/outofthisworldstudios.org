import express from "express";
import queries from "./lib/dbQueries.mjs";
import { generateThumbnailImageName } from "./lib/generateThumbnails.mjs";

const filesTableName = process.env.FILES_TABLE;

const router = express.Router();

router.get("/", async (req, res) => {
  try {
    const params = {
      TableName: filesTableName,
      ConsistentRead: false,
    };
    const videos = await queries.scan(params);
    videos.Items.forEach((video, idx, arry) => {
      arry[idx].thumbnail = generateThumbnailImageName(video.videoUrl, 0);
    });


    videos.Items.sort(function compare(a, b) {
      let dateB = new Date(b.dateString);
      let dateA = new Date(a.dateString);
      return dateA - dateB;
    });
    res.json(videos);
  } catch (error) {
    res.status(500).json(error);
  }
});

router.get("/active", async (req, res) => {
  try {
    const params = {
      TableName: filesTableName,
      ConsistentRead: false,
      FilterExpression: "isActive = :isActiveValue",
      ExpressionAttributeValues: {
        ":isActiveValue": true,
      },
    };
    const videos = await queries.scan(params);
    videos.Items.forEach((video, idx, arry) => {
      arry[idx].thumbnail = generateThumbnailImageName(video.videoUrl, 0);
    });
    videos.Items.sort(function compare(a, b) {
      const dateA = new Date(a.dateString);
      const dateB = new Date(b.dateString);
      return dateA - dateB;
    });

    res.json(videos);
  } catch (error) {
    res.status(500).json(error);
  }
});

export default router;
