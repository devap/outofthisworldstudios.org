import express from "express";

import queries from "./lib/dbQueries.mjs";
import { signInToken } from "./lib/jwt.mjs";

const router = express.Router();

router.post("/:path", async (req, res) => {
  let table = "";
  switch (req.params.path) {
    case "admin":
      table = process.env.ADMIN_TABLE;
      break;
    case "user":
      table = process.env.USER_TABLE;
      break;
  }
  const params = {
    TableName: table,
    FilterExpression: "username = :username AND password = :password",
    ExpressionAttributeNames: { "#r": "role" },
    ExpressionAttributeValues: {
      ":username": req.body.username,
      ":password": req.body.password,
    },
    ProjectionExpression: "username, #r",
  };

  queries.scan(params).then(
    (data) => {
      if (data.Items.length) {
        const jwtData = {
          username: data.Items[0].username,
          role: data.Items[0].role,
        };
        res.json({
          message: "login Successful",
          data: jwtData,
          token: signInToken(jwtData),
        });
      } else {
        res.status(401).json({
          message: "Invalid Credentials",
        });
      }
    },
    (error) => {
      res.status(500).json({ error });
    }
  );
});

export default router;
