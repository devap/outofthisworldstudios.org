import express from "express";
import { verifyToken } from "./jwt.mjs";

import { getSignedUrl } from "@aws-sdk/s3-request-presigner";
import { PutObjectCommand, S3Client } from "@aws-sdk/client-s3";

const router = express.Router();

const clientParams = {
  region: "us-east-1",
};
const s3c = new S3Client(clientParams);

router.post("/", verifyToken, async (req, res) => {
  const putObjectParams = {
    Bucket: process.env.FILE_UPLOAD_BUCKET,
    Key: "videos/" + req.body.fileName,
    ContentType: req.body.contentType,
  };
  const command = new PutObjectCommand(putObjectParams);
  try {
    // const uploadURL = await s3c.getSignedUrlPromise('putObject', s3Params)
    const uploadURL = await getSignedUrl(s3c, command, { expiresIn: 3600 });
    res.json({
      uploadURL: uploadURL,
      // putObjectParams: putObjectParams
    });
  } catch (error) {
    // error handling.
    console.log("error with  getSignedUrl(): ", error);
    console.log("s3Params: ", putObjectParams);
  }
});

export default router;
