import { CloudFrontClient, CreateInvalidationCommand } from '@aws-sdk/client-cloudfront';

const createInvalidation = async (distributionId, path) => {
  const client = new CloudFrontClient();

  const params = {

    DistributionId: distributionId,
    InvalidationBatch: {
      CallerReference: String(new Date().getTime()),
      Paths: {
        Quantity: 1,
        Items: [path]
      }
    }
  }

  const createInvalidationCommand = new CreateInvalidationCommand(params)
  const response = await client.send(createInvalidationCommand)
  console.log('Posted cloudfront invalidation, response:', response)

  return response
}

export {
  createInvalidation
}
