import fs from "fs";
import path from "path";
import { spawnSync } from "child_process";

const ffprobePath = "/opt/bin/ffprobe";

const getRandomNumber = (upperLimit) => {
  return Math.floor(Math.random() * upperLimit);
};

const getRandomString = (len) => {
  const charset =
    "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
  let result = "";

  for (let i = len; i > 0; --i) {
    result += charset[Math.floor(Math.random() * charset.length)];
  }

  return result;
};

const doesFileExist = (filePath) => {
  if (fs.existsSync(filePath)) {
    const stats = fs.statSync(filePath);
    const fileSizeInBytes = stats.size;

    if (fileSizeInBytes > 0) {
      return true;
    } else {
      console.error(`${filePath} exists but is 0 bytes in size`);
      return false;
    }
  } else {
    console.error(`${filePath} does not exist`);
    return false;
  }
};

const generateRandomTimes = (tmpVideoPath, numberOfTimesToGenerate) => {
  const timesInSeconds = [];
  const videoDuration = getVideoDuration(tmpVideoPath);

  for (let x = 0; x < numberOfTimesToGenerate; x++) {
    const randomNum = getRandomNumberNotInExistingList(
      timesInSeconds,
      videoDuration
    );

    if (randomNum >= 0) {
      timesInSeconds.push(randomNum);
    }
  }

  return timesInSeconds;
};

const getRandomNumberNotInExistingList = (existingList, maxValueOfNumber) => {
  for (let attemptNumber = 0; attemptNumber < 3; attemptNumber++) {
    const randomNum = getRandomNumber(maxValueOfNumber);

    if (!existingList.includes(randomNum)) {
      return randomNum;
    }
  }

  return -1;
};

const extractParams = (event) => {
  const videoPathFileName = decodeURIComponent(
    event.Records[0].s3.object.key
  ).replace(/\+/g, " ");
  const triggerBucketName = event.Records[0].s3.bucket.name;

  return { videoPathFileName, triggerBucketName };
};

const getVideoDuration = (tmpVideoPath) => {
  const ffprobe = spawnSync(ffprobePath, [
    "-v",
    "error",
    "-show_entries",
    "format=duration",
    "-of",
    "default=nw=1:nk=1",
    tmpVideoPath,
  ]);

  return Math.floor(ffprobe.stdout.toString());

};

const wipeTmpDirectory = async () => {
  const files = await fs.promises.readdir("/tmp/");
  const filePaths = files.map((file) => path.join("/tmp/", file));
  await Promise.all(filePaths.map((file) => fs.promises.unlink(file)));
};

export {
  doesFileExist,
  extractParams,
  getRandomString,
  generateRandomTimes,
  getRandomNumberNotInExistingList,
  wipeTmpDirectory,
};
