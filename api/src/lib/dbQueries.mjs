import {
  DeleteItemCommand,
  DynamoDBClient,
  PutItemCommand,
  ScanCommand,
  UpdateItemCommand,
} from "@aws-sdk/client-dynamodb";

import { marshall, unmarshall } from "@aws-sdk/util-dynamodb";

const clientParams = {
  region: "us-east-1",
};
if (process.env.IS_OFFLINE) {
  console.log("IS_OFFLINE");
  clientParams.region = "localhost";
  clientParams.endpoint = "http://localhost:8000";
}

const dbClient = new DynamoDBClient(clientParams);

const scan = async (params) => {
  if ("ExpressionAttributeValues" in params) {
    params["ExpressionAttributeValues"] = marshall(
      params["ExpressionAttributeValues"]
    );
  }
  try {
    const command = new ScanCommand(params);
    const response = await dbClient.send(command);

    if ("Items" in response) {
      response["Items"].forEach((item, idx, arry) => {
        arry[idx] = unmarshall(item);
      });
    }

    return response;
  } catch (error) {
    console.log("dbQueries.mjs->scan()->error", error);
  }
};

const put = async (params) => {
  params["Item"] = marshall(params["Item"]);

  const command = new PutItemCommand(params);
  const response = await dbClient.send(command);
  console.log("dbQueries.mjs->put()->response", response);
  return response;
};

const update = async (params) => {
  params["Key"] = marshall(params["Key"]);
  params["ExpressionAttributeValues"] = marshall(
    params["ExpressionAttributeValues"]
  );
  const command = new UpdateItemCommand(params);
  return await dbClient.send(command);
};

const deleteItem = async (params) => {
  params["Key"] = marshall(params["Key"]);
  const command = new DeleteItemCommand(params);
  return await dbClient.send(command);
};

export default {
  scan,
  put,
  deleteItem,
  update,
};
