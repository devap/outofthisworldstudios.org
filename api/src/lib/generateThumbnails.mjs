import fs from "fs";
import {S3} from "@aws-sdk/client-s3";
import {Upload} from "@aws-sdk/lib-storage";
import {spawnSync} from "child_process";
import {doesFileExist, generateRandomTimes, getRandomString,} from "./utils.mjs";

const ffmpegPath = "/opt/bin/ffmpeg";

const THUMBNAIL_TARGET_BUCKET = process.env.FILE_UPLOAD_BUCKET;

// const getFilePathTemplate = (filePathTemplate) => {
//   const hash = getRandomString(10);
//   return filePathTemplate.replace("{HASH}", hash);
// };

const generateThumbnailImageName = (filename, idx) => {
  const basename = filename.substring(0, filename.lastIndexOf("."));
  return `${basename}-${idx}.jpg`;
};

const generateThumbnailPath = (targetSecond) => {
  const tmpThumbnailPathTemplate = "/tmp/thumbnail-{HASH}-{num}.jpg";
  const hash = getRandomString(10);
  const uniqueThumbnailPath = tmpThumbnailPathTemplate.replace("{HASH}", hash);
  return uniqueThumbnailPath.replace("{num}", targetSecond);
};

const saveFileToTmpDirectory = async (fileAsBuffer, videoPathFileName) => {
  const hash = getRandomString(10);
  const _ext = videoPathFileName.split(".").pop();
  const tmpVideoPathTemplate = "/tmp/vid-{HASH}." + _ext;
  const tmpVideoFilePath = tmpVideoPathTemplate.replace("{HASH}", hash);

  await fs.promises.writeFile(tmpVideoFilePath, fileAsBuffer, "base64");

  return tmpVideoFilePath;
};

const downloadVideo2TmpDir = async (triggerBucketName, videoPathFileName) => {
  const downloadResult = await getVideoFromS3(triggerBucketName, videoPathFileName);
  const videoAsBuffer = downloadResult.Body;
  return await saveFileToTmpDirectory(videoAsBuffer, videoPathFileName);
};

const createImageFromVideo = (tmpVideoPath, targetSecond) => {
  const tmpThumbnailPath = generateThumbnailPath(targetSecond);
  const ffmpegParams = createFfmpegParams(
    tmpVideoPath,
    tmpThumbnailPath,
    targetSecond
  );
  spawnSync(ffmpegPath, ffmpegParams);

  return tmpThumbnailPath;
};

const createFfmpegParams = (tmpVideoPath, tmpThumbnailPath, targetSecond) => {
  return [
    "-ss",
    targetSecond,
    "-i",
    tmpVideoPath,
    // "-vf", "thumbnail,scale=108:108",
    "-vf",
    "scale=iw*sar:ih,setsar=1",
    "-vframes",
    1,
    tmpThumbnailPath,
  ];
};

const uploadFileToS3 = async (tmpThumbnailPath, nameOfImageToCreate) => {
  const contents = fs.createReadStream(tmpThumbnailPath);
  const uploadParams = {
    Bucket: THUMBNAIL_TARGET_BUCKET,
    Key: nameOfImageToCreate,
    Body: contents,
    ContentType: "image/jpg",
  };

  const s3 = new S3();

  try {
    await new Upload({
      client: s3,
      params: uploadParams,
    }).done();
  } catch (error) {
    return error;
  }
};

const getVideoFromS3 = async (triggerBucketName, fileName) => {
  const s3 = new S3();
  return await s3.getObject({
    Bucket: triggerBucketName,
    Key: fileName,
  });
};

const generateThumbnailsFromVideo = async (
  tmpVideoPath,
  numberOfThumbnails,
  videoFileName
) => {
  const randomTimes = generateRandomTimes(tmpVideoPath, numberOfThumbnails);

  for (const [idx, randomTime] of Object.entries(randomTimes)) {
    const tmpThumbnailPath = createImageFromVideo(tmpVideoPath, randomTime);

    if (doesFileExist(tmpThumbnailPath)) {
      const nameOfImageToCreate = generateThumbnailImageName(videoFileName, idx);
      await uploadFileToS3(tmpThumbnailPath, nameOfImageToCreate);
    }
  }
};

export {
  downloadVideo2TmpDir,
  generateThumbnailImageName,
  generateThumbnailsFromVideo,
};
