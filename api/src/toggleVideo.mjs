import express from "express";

import queries from "./lib/dbQueries.mjs";
import { verifyToken } from "./lib/jwt.mjs";
import {createInvalidation} from "./lib/cloudfront.mjs";

const router = express.Router();
const filesTableName = process.env.FILES_TABLE;

router.post("/", verifyToken, async (req, res) => {
  const params = {
    TableName: filesTableName,
    Key: {
      dateString: req.body.dateString,
    },
    UpdateExpression: "set isActive = :isActiveValue",
    ExpressionAttributeValues: {
      ":isActiveValue": req.body.isActive,
    },
  };

  try {

    // update dynamodb
    await queries.update(params);

    // invalidate cloudfront cache
    await createInvalidation(process.env.CLOUDFRONT_DISTRIBUTION_ID, "/api/getvideos/active");

    // return success
    res.status(200).json({ message: "Toggled Active Status" });

  } catch (error) {

    console.log("toggleVideo.mjs->post()->Failed to toggle: ", error);
    res.status(500).json({ message: "Failed to toggle", error });

  }
});

export default router;
