import app from "./app.mjs";
import serverless from "serverless-http";
import {doesFileExist, extractParams, wipeTmpDirectory} from "./lib/utils.mjs";
import {downloadVideo2TmpDir, generateThumbnailsFromVideo,} from "./lib/generateThumbnails.mjs";

const THUMBNAILS_TO_CREATE = 1;

const appHandler = serverless(app);

const thumbHandler = async (event) => {
  // console.log("index.mjs->thumbHandler()->event", event);
  const {videoPathFileName, triggerBucketName} = extractParams(event);
  console.log('index.mjs->thumbHandler()->videoPathFileName', videoPathFileName);

  // Check event for .mp4/.mov files only, otherwise return
  if (!/.mov|.mp4/.test(videoPathFileName)) {
    console.log('We DON\'t have a match, exiting...');
    return
  } else {
    console.log('We DO have a match - proceeding...');
  }

  await wipeTmpDirectory();

  const tmpVideoPath = await downloadVideo2TmpDir(
    triggerBucketName,
    videoPathFileName
  );
  console.log("index.mjs->thumbHandler()->tmpVideoPath", tmpVideoPath);

  if (doesFileExist(tmpVideoPath)) {
    await generateThumbnailsFromVideo(
      tmpVideoPath,
      THUMBNAILS_TO_CREATE,
      videoPathFileName
    );
  }

};

export {
  appHandler,
  thumbHandler
};
