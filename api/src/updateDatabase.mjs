import crypto from "crypto";
import express from "express";

import queries from "./lib/dbQueries.mjs";
import { verifyToken } from "./lib/jwt.mjs";
import { createInvalidation } from "./lib/cloudfront.mjs";

const router = express.Router();
const filesTableName = process.env.FILES_TABLE;

router.post("/", verifyToken, async (req, res) => {
  const item = {
    id: crypto.randomUUID(),
    date: new Date().toISOString(),
    time: new Date().toTimeString(),
    title: req.body.title,
    dateString: req.body.date,
    videoUrl: req.body.url,
    isActive: req.body.isActive,
  };
  const params = {
    TableName: filesTableName,
    Item: item,
  };
  try {
    const result = await queries.put(params);
    if (result) {
      // invalidate cloudfront cache
      await createInvalidation(process.env.CLOUDFRONT_DISTRIBUTION_ID, "/api/getvideos/active");

      res.json({
        message: "uploaded successfully",
        url: req.body.url,
      });


    }
  } catch (error) {
    console.log("updateDatabase.mjs->router.post()->error: ", error);
    res.status(500).json(error);
  }
});

export default router;
