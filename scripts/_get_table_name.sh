if [ $# -lt 1 ]
then
  me=$(basename "$0")
  echo "USAGE: $me <table_name>"
  exit 1
fi

TABLE=$1
