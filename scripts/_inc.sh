set -o nounset
set -o errexit
set -o pipefail

LOCAL=true
ENDPOINT="--endpoint-url http://localhost:8000"

echo "Use Local DynamoDB? [Y/n]"
read -e get_env
if [ "$get_env" = "N" ] || [ "$get_env" = "n" ]
then
    ENDPOINT=""
fi

echo "LOCAL: ${LOCAL}"
echo "ENDPOINT: ${ENDPOINT}"
