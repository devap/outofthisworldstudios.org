import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  isLoading = new BehaviorSubject(false);

  constructor(private readonly http: HttpClient) {}

  get(path: string): Observable<any> {
    const url = `${environment.baseapiUrl}${path}`;
    return this.http.get(url);
  }

  delete(path: string): Observable<any> {
    const url = `${environment.baseapiUrl}${path}`;
    return this.http.delete(url);
  }

  post(path: string, body: any): Observable<any> {
    const url = `${environment.baseapiUrl}${path}`;
    console.log('api.service.ts->post()->url', url);
    return this.http.post(url, body);
  }

  s3put(url: string, contentType: any, payload: any): Observable<any> {
    const headers = new HttpHeaders({
      'Content-Type': contentType,
    });

    return this.http.put(url, payload, { headers: headers });
  }
}
