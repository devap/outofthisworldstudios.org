import { Component, OnInit } from '@angular/core';
import { ApiService } from './services/api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  isLoading = true;
  // title = 'ui';

  constructor(private readonly apiService: ApiService) {}

  ngOnInit(): void {
    this.apiService.isLoading.subscribe((value) => {
      setTimeout(() => {
        this.isLoading = value;
      });
    });
  }
}
