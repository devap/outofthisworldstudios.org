import { Injectable } from '@angular/core';
import { ApiService } from '../services/api.service';
import { Observable } from 'rxjs';
import { urls } from '../urls/urls';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private readonly apiService: ApiService) {}

  // getRecentVideo(): Observable<any> {
  //     const path = `${urls.getRecentVideo}`;
  //     return this.apiService.get(path);
  // }

  getActiveVideos(): Observable<any> {
    const path = `${urls.getActiveVideos}`;
    return this.apiService.get(path);
  }

  // getInitialVideo(): Observable<any> {
  //   const path = `${urls.getInitialVideo}`;
  //   return this.apiService.get(path);
  // }
  //
  // getAllVideos(): Observable<any> {
  //     const path = `${urls.getAllVideos}`;
  //     return this.apiService.get(path);
  // }
}
