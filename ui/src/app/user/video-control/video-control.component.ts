import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../user.service';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-video-control',
  templateUrl: './video-control.component.html',
  styleUrls: ['./video-control.component.scss'],
})

// eslint-disable-next-line import/prefer-default-export
export class VideoControlComponent implements OnInit {
  allVideos: any[] = [];
  selectedVideo: any = {};
  allVideosLoaded: boolean = false;
  selectedVideoLoaded: boolean = false;
  private errorMessage = '';

  // page: any;

  constructor(
    private route: ActivatedRoute,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    this.loadInitialVideo();
    this.getActiveVideos();
  }

  getDivID(id: string): string {
    return 'v' + id.split('-')[0];
  }

  highlightCurrentPlaylistItem(divID: string): void {
    const playlistItem = document.querySelector('#' + divID)!;
    playlistItem.classList.add('playlist-item');
  }

  loadInitialVideo(): void {
    let _initialVideo: any = {
      date: '2022-01-04T14:46:46.861Z',
      videoUrl: 'https://' + `${environment.cdnHost}` + '/videos/01.mp4',
      dateString: '01/25/2021',
      time: '14:46:46 GMT+0000 (Coordinated Universal Time)',
      id: 'e8e74f0e-0296-4059-84b8-d17c723f23a2',
      isActive: true,
      title: 'First Meeting with Srila Prabhupada',
      thumbnail: 'https://' + `${environment.cdnHost}` + '/videos/01-0.jpg',
    };
    _initialVideo['divID'] = this.getDivID(_initialVideo.id);
    this.selectedVideo = _initialVideo;
    this.selectedVideoLoaded = true;
  }

  getActiveVideos(): void {
    let _allVideos: any[] = [];
    this.userService.getActiveVideos().subscribe(
      (success) => {
        if (success) {
          _allVideos = success.Items;

          _allVideos.forEach((video, idx, arry) => {
            // We're appending a time slice to all the videos so the poster frame only is downloaded at first
            const _url = new URL(video.videoUrl);
            _url.hostname = `${environment.cdnHost}`;
            // _url.hash = '#t=0.1';
            _url.pathname = _url.pathname.replace(
              `/${environment.cdnHost}`,
              ''
            );

            // create an id to use for the div
            arry[idx].divID = this.getDivID(video.id);
            arry[idx].videoUrl = _url.href;
          });

          this.allVideos = _allVideos;
          this.allVideosLoaded = true;

          setTimeout(() => {
            this.highlightCurrentPlaylistItem(this.selectedVideo.divID);
          }, 500);
        } else {
          console.log('oops');
        }
      },
      (error) => {
        this.errorMessage = 'Invalid Credentials';
        setTimeout(() => {
          this.errorMessage = '';
        }, 9000);
        console.log(this.errorMessage);
      }
    );
  }

  newVideoWatch(video: any): void {
    this.selectedVideo = {};

    // we need a pause here for reasons I don't fully understand
    setTimeout(() => {
      this.selectedVideo = video;

      // pause again so the <video> element can be initialized
      setTimeout(() => {
        const playerElement = <HTMLVideoElement>(
          document.querySelector('#videoPlayer')!
        );
        playerElement.setAttribute('muted', 'false');
        let state = playerElement.play();
        console.log('playerElementAfter', playerElement);
      }, 200);
    }, 200);

    // scroll playlist into view
    const playlistElement = document.querySelector('#' + video.divID)!;
    playlistElement.scrollIntoView({
      behavior: 'smooth',
      block: 'start',
    });

    // scroll main video into view
    const videoElement = document.querySelector('#video-player')!;
    videoElement.scrollIntoView({
      behavior: 'smooth',
    });

    // remove highlight from any previously selected item in the playlist
    // const playlistItems = document.querySelectorAll('.video-card a');
    const playlistItems = document.querySelectorAll('.playlist-item')!;
    playlistItems.forEach((item) => {
      item.classList.remove('playlist-item');
    });

    // highlight the selected item in the playlist
    this.highlightCurrentPlaylistItem(video.divID);
  }
}
