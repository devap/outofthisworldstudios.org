import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserComponent } from './user.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { VideoControlComponent } from './video-control/video-control.component';

const routes: Routes = [
  {
    path: '', component: UserComponent, children: [
      { path: '', component: VideoControlComponent },
      { path: 'about-us', component: AboutUsComponent },
      { path: '**', redirectTo: '' }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
