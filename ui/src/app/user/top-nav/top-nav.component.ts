import { Component, OnInit } from '@angular/core';
import { ClarityIcons, barsIcon, timesIcon } from '@cds/core/icon';
ClarityIcons.addIcons(barsIcon, timesIcon);

@Component({
  selector: 'app-top-nav',
  templateUrl: './top-nav.component.html',
  styleUrls: ['./top-nav.component.scss']
})
export class TopNavComponent implements OnInit {
  isMenuOpen = false;
  constructor() {}

  ngOnInit(): void {}

}
