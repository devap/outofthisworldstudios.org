import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { ClarityModule } from '@clr/angular';

import { UserRoutingModule } from './user-routing.module';
import { UserComponent } from './user.component';
import { TopNavComponent } from './top-nav/top-nav.component';
import { FooterComponent } from './footer/footer.component';
import { VideoControlComponent } from './video-control/video-control.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { UserService } from './user.service';

@NgModule({
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [
    UserComponent,
    TopNavComponent,
    FooterComponent,
    VideoControlComponent,
    AboutUsComponent,
  ],
  imports: [
    CommonModule,
    UserRoutingModule,
    // ClarityModule,
  ],
  providers: [UserService],
})
export class UserModule {}
