export const urls = {
    homePage: '/home',
    adminLogin: 'login/admin',
    updateDatabase: 'updatedatabase',
    getAllVideos: 'getvideos',
    getActiveVideos: 'getvideos/active',
    // getInitialVideo: 'getvideos/initial',
    // getRecentVideo: 'getvideos/recent',
    getSignedUrl: 'getsignedurl',
    deleteVideo: 'deletevideo',
    toggleVideo: 'togglevideo',
}
