import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminGuard } from './admin.guard';
import { AdminComponent } from './admin.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { UploadVideoComponent } from './upload-video/upload-video.component';
import { VideosListComponent } from './videos-list/videos-list.component';

const routes: Routes = [
  { path: '', component: AdminComponent, children: [
    { path: 'login', component: LoginComponent },
    { path: 'home', component: HomeComponent, canActivate: [AdminGuard], children: [
      { path: 'upload', component: UploadVideoComponent },
      { path: 'videos-list', component: VideosListComponent }
    ]},
    { path: '**', redirectTo: 'login' }
  ] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
