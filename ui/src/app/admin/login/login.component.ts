import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AdminService } from '../services/admin.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  adminForm!: FormGroup;
  errorMessage = '';

  constructor(private readonly fb: FormBuilder,
    private readonly adminService: AdminService,
    private readonly router: Router) { }

  ngOnInit(): void {
    this.adminForm = this.initAdminForm();
  }

  initAdminForm() {
    return this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    })
  }

  login(): void {
    if (this.adminForm.valid) {
      this.adminService.login(this.adminForm.value).subscribe(success => {
        localStorage.setItem('token', success.token);
        this.router.navigate(['/admin/home/upload']);
      }, error => {
        this.errorMessage = 'Invalid Credentials';
        console.log(error);
      });
    }
  }

}
