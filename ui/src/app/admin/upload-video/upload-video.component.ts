import { Component, OnInit } from '@angular/core';
import { AdminService } from '../services/admin.service';
import { Router } from '@angular/router';

import '@cds/core/button/register.js';

@Component({
  selector: 'app-upload-video',
  templateUrl: './upload-video.component.html',
  styleUrls: ['./upload-video.component.scss'],
})
export class UploadVideoComponent implements OnInit {
  file!: any;
  message: any = {};
  date: any;
  isRequired = false;
  title = '';
  isActive = false;
  loadingState = 'default';
  btnStatus = 'primary';

  constructor(
    private readonly adminService: AdminService,
    private readonly router: Router
  ) {}

  ngOnInit(): void {}

  submitReset(): void {
    this.loadingState = 'default';
    this.btnStatus = 'primary';
    this.isRequired = false;
    this.message = {};
    console.log('this.isRequired:', this.isRequired);
  }

  onFileSelect(event: any): void {
    this.file = event.target.files[0];
    this.submitReset();
  }

  submit(): void {
    if (this.date && this.file) {
      this.isRequired = false;
      this.showMessage('uploading');
      if (this.title === '') {
        this.title = this.file.name;
      }
      this.adminService
        .getSignedUrl({ contentType: this.file.type, fileName: this.file.name })
        .subscribe(
          (response) => {
            this.adminService
              .videoToS3(response.uploadURL, this.file.type, this.file)
              .subscribe(
                (responseS3) => {
                  const tablePayload = {
                    url: response.uploadURL.substring(
                      0,
                      response.uploadURL.indexOf('?')
                    ),
                    date: this.date,
                    title: this.title,
                    key: response.Key,
                    isActive: this.isActive,
                  };
                  console.log('tablePayload', tablePayload);

                  this.adminService.updateDatabase(tablePayload).subscribe(
                    (success) => {
                      this.showMessage('success');
                    },
                    (error) => {
                      this.showMessage('error');
                    }
                  );
                },
                (error) => {
                  this.showMessage('error');
                }
              );
          },
          (error) => {
            this.showMessage('error');
          }
        );
    } else {
      this.isRequired = true;
    }
  }

  showMessage(msg: string): void {
    if (msg === 'success') {
      this.loadingState = 'success';
      this.btnStatus = 'success';
      this.message = {
        class: 'success-class',
        text: 'Successfully Uploaded',
      };
    }
    if (msg === 'uploading') {
      this.loadingState = 'loading';
      this.btnStatus = 'neutral';
      this.message = {
        class: 'uploading-class',
        text: 'Uploading',
      };
    }

    if (msg === 'error') {
      this.loadingState = 'error';
      this.btnStatus = 'danger';
      this.message = {
        class: 'error-class',
        text: 'Failed to Upload',
      };
    }
  }
}
