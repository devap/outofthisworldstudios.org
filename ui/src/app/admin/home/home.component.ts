import { Component, OnInit } from '@angular/core';
import { ClarityIcons, userIcon } from '@cds/core/icon';
import { Router } from '@angular/router';

ClarityIcons.addIcons(userIcon);

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  open = false;

  constructor(private readonly router: Router) {}

  ngOnInit(): void {}

  logout(): void {
    localStorage.clear();
    this.router.navigate(['/admin']);
  }

}
