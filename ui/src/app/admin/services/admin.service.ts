import { Injectable } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { Observable } from 'rxjs';
import { urls } from '../../urls/urls';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor(private readonly apiService: ApiService) { }

  login(payload: any): Observable<any> {
    const path = `${urls.adminLogin}`;
    return this.apiService.post(path, payload);
  }

  updateDatabase(payload: any): Observable<any> {
    const path = `${urls.updateDatabase}`;
    return this.apiService.post(path, payload);
  }

  getSignedUrl(payload: any): Observable<any> {
    const path = `${urls.getSignedUrl}`;
    return this.apiService.post(path, payload);
  }

  videoToS3(url: string, contentType: any, payload: any): Observable<any> {
    return this.apiService.s3put(url, contentType, payload);
  }

  // getVideoListActive(): Observable<any> {
  //   const url = urls.getActiveVideos;
  //   return this.apiService.get(url);
  // }

  getVideoListAll(): Observable<any> {
    const url = urls.getAllVideos;
    return this.apiService.get(url);
  }

  deleteVideo(payload: any): Observable<any> {
    const url = `${urls.deleteVideo}`;
    return this.apiService.post(url, payload);
  }

  toggleVideo(payload: any): Observable<any> {
    const url = `${urls.toggleVideo}`;
    return this.apiService.post(url, payload);
  }

}
