import { Component, OnInit } from '@angular/core';
import { AdminService } from '../services/admin.service';
import { ClarityIcons, trashIcon } from '@cds/core/icon';

ClarityIcons.addIcons(trashIcon);

@Component({
  selector: 'app-videos-list',
  templateUrl: './videos-list.component.html',
  styleUrls: ['./videos-list.component.scss'],
})
export class VideosListComponent implements OnInit {
  videoList: any;
  openModal = false;
  selectedVideo: any;

  constructor(private readonly adminservice: AdminService) {}

  ngOnInit(): void {
    this.getAllVideos();
  }

  toggleActiveVideo(video: any): void {
    this.selectedVideo = video;
    video.isActive = !video.isActive;
    this.adminservice.toggleVideo(this.selectedVideo).subscribe(
      (res) => {
        this.getAllVideos();
      },
      (error) => {
        console.log(error);
      }
    );
  }

  getAllVideos(): void {
    this.adminservice.getVideoListAll().subscribe(
      (response) => {
        this.videoList = response.Items;
        console.log(
          'video-list-component.ts->getAllVideos()->videoList: ',
          this.videoList
        );
      },
      (error: any) => {
        console.log('video-list-component.ts->getAllVideos()->error: ', error);
      }
    );
  }

  deleteVideo(video: any): void {
    this.openModal = true;
    this.selectedVideo = video;
  }

  delete(): void {
    this.openModal = false;
    this.adminservice.deleteVideo(this.selectedVideo).subscribe(
      (response) => {
        console.log(response);
        this.getAllVideos();
      },
      (error) => {
        console.log(error);
      }
    );
  }
}
