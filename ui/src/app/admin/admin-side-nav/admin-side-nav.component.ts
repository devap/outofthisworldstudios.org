import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin-side-nav',
  templateUrl: './admin-side-nav.component.html',
  styleUrls: ['./admin-side-nav.component.scss']
})
export class AdminSideNavComponent implements OnInit {

  navItems: any = [
    { title: 'Upload Video', routerLink: '/admin/home/upload' },
    { title: 'List Videos', routerLink: '/admin/home/videos-list' },
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
