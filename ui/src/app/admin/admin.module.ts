import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClarityModule } from '@clr/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { AdminSideNavComponent } from './admin-side-nav/admin-side-nav.component';
import { UploadVideoComponent } from './upload-video/upload-video.component';
import { VideosListComponent } from './videos-list/videos-list.component';

@NgModule({
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [
    AdminComponent,
    LoginComponent,
    HomeComponent,
    AdminSideNavComponent,
    UploadVideoComponent,
    VideosListComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    AdminRoutingModule,
    FormsModule,
    ClarityModule,
  ],
})
export class AdminModule {}
