export const environment = {
  production: false,
  baseapiUrl: 'https://test.outofthisworldstudios.org/api/',
  // baseapiUrl: 'https://7062sk55u3.execute-api.us-east-1.amazonaws.com/api/',
  originHost: 's3.us-east-1.amazonaws.com',
  cdnHost: 'test.outofthisworldstudios.org',
};
