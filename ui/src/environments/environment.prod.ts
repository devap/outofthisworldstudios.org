export const environment = {
  production: true,
  baseapiUrl: 'https://outofthisworldstudios.org/api/',
  originHost: 's3.us-east-1.amazonaws.com',
  cdnHost: 'outofthisworldstudios.org',
};
