# OutOfThisWorldStudios.org

There are three parts to this project:

1. Backend (api) NodeJS backend contains the Lambda function
    - api: provides rest apis for retrieving data from DynamoDB
    - thumbnail: auto-generates thumbnails when images are uploaded to S3
1. Frontend (ui): Angular 12 frontend
   - user: user-facing website
   - admin: admin site for uploading & managing videos

Components:

## Development
The lambda functions can be run locally using the Serverless Framework.

1. Start the DynamoDB local instance:

```
$ cd api
$ yarn db
```

(This runs the following command):

```
$ cd .dynamodb
$ java -Djava.library.path=./DynamoDBLocal_lib -jar DynamoDBLocal.jar -sharedDb"
```

There is also a Docker alternative, but we need to sort out where the data is stored:

```
$ docker run -p 8000:8000 amazon/dynamodb-local (or cd docker; docker-compose up)
```

    Ref: https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/workbench.html

There are scripts in the `scripts` directory for creating and seeding the database.

There is a GUI for DynamoDB available at http://localhost:8000/shell
There is a DynamoDB admin tool available at: https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/workbench.html

2. Start the api locally:

```
$ cd api
$ yarn dev (runs `sls offline start`)
```

3. To test the api locally:

```
$ sls invoke local -f api -p src/fixtures/uploadVideo-test-mp4.json
```

4. The frontend can be run locally using the Angular CLI.

```
$ cd ui
$ yarn serve
```

Then open http://localhost:4200 in your browser.

## Deployment

Backend is located in `api` directory and is deployed via Serverless Framework to AWS Lambda.

```
$ cd ./api
$ sls deploy -s prod
$ sls deploy -s test
$ sls deploy function -f api -s test
$ sls deploy function -f thumbnail -s test
```

Frontend is located in the `ui` directory and is deployed via Github CI/CD (.gitlab-ci.yml) to the following locations:

- dev: GitHub Pages (dev.outofthisworldstudios.org)
- test: S3 (test.outofthisworldstudios.org)
- prod: S3 (outofthisworldstudios.org)

## Lambda Warmup
The Serverless Framework plugin `serverless-plugin-warmup` is used to keep the `api` Lambda functions warm.
Ref: https://www.serverless.com/plugins/serverless-plugin-warmup

## CloudFront "Access Denied" Error (XML doc)
One important things is left. Angular has routing and URL rewriting concept inside. Means one page does all routing. index.html is root file which is responsible for all routing. This above setup will works when your continuously used application without refreshing. If hit refresh then it will give "Access Denied" from S3 bucket.

Answer is S3 doesn't understand route open when you reload and open in new tab. Have to inform S3 is for this route used index.html. Whenever new route open its gives 403 [access denied ] error. for this you need to do setting CloudFront to set 403 error page redirect to index.html.
![cloudfront.png](cloudfront.png)
